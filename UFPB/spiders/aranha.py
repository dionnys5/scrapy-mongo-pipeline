# -*- coding: utf-8 -*-
import scrapy
from ..items import UfpbItem


class AranhaSpider(scrapy.Spider):
    name = 'aranha'
    start_urls = ['https://repositorio.ufpb.br/jspui/simple-search?location=&query=saude+mental']
    custom_settings = {
        'ITEM_PIPELINES': {
            'UFPB.pipelines.UfpbPipeline': 400
        },
        'LOG_FILE': 'logs\\tutorial.log',
        'FEED_FORMAT': 'json',
        'JOBDIR': 'crawls\\upfb_02_08_2019',
        'FEED_URI': 'ufpb.json'
    }

    def parse(self, response):
        lista = response.css('td:nth-child(2) a::attr(href)').extract()
        next_page= response.css('.pagination li:last-child a::attr(href)').get()
        for link in lista:
            yield response.follow(link, self.Artigo)
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)
    def Artigo(self, response):
        itens = UfpbItem()

        titulo = response.css(':nth-child(3) tr:nth-child(2) td.metadataFieldValue::text').extract()
        resumo = response.css(':nth-child(3) tr:nth-child(5) td.metadataFieldValue::text').extract()
        
        itens['titulo'] = titulo
        itens['resumo'] = resumo
        

        yield itens
