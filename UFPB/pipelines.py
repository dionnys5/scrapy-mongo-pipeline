# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymongo
import logging
import datetime


class UfpbPipeline(object):

    collection_name = 'TrabalhoAcademico'
    database = 'Proict'

    def __init__(self):
        self.client = pymongo.MongoClient()
        self.db = self.client[self.database]


    def close_spider(self, spider):
        #fechar conexão quando spider terminar
        self.client.close()

    def process_item(self, item, spider):
        # Como inserir cada item no banco
        # É Possível criar a mesma estrutura no Item e apenas convertelo para dict()
        print(item['titulo'][0])
        trabalho = {
            'repositorio': 'ufpb',
            'titulo': item['titulo'][0],
            'resumo': item['resumo'][0],
            'data': datetime.datetime(2019, 8, 5),
            'url': 'https://www.repositorio.com',
            'autores': ['autor1', 'autor2', 'autor3'],
            'palavras-chave': ['palavra1', 'palavra2']
        }
        self.db[self.collection_name].insert(trabalho)
        logging.debug("Trabalho adicionado ao MongoDB")
        return item
